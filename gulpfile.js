var gulp            = require('gulp');
var sass            = require('gulp-sass');
var concat          = require('gulp-concat');
var less            = require('gulp-less');
var postcss         = require('gulp-postcss');
var jsminify        = require('gulp-terser');
var cssminify       = require('gulp-clean-css');

var paths = {
    styles: {
        src: 'scss/*.scss',
        dest: 'htdocs/'
    },
    scripts: {
        src: 'js/*.js',
        dest: 'htdocs/'
    }
};

function styles() {
    return gulp.src(paths.styles.src)
        .pipe(sass())
        .pipe(cssminify())
        .pipe(gulp.dest(paths.styles.dest));
}
function scripts() {
    return gulp.src(['node_modules/jquery/dist/jquery.slim.js',
        'js/popper.js',
        'node_modules/bootstrap/dist/js/bootstrap.js'])
        .pipe(concat('main.js'))
        .pipe(jsminify())
        .pipe(gulp.dest(paths.scripts.dest));
}

exports.styles  = styles;
exports.scripts = scripts;